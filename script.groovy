def buildJar(){
    echo "build the application..."
    sh 'mvn package'
}

def buildimage() {
    echo "build the docker image..."
                withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                
                  sh 'docker build -t shikalgarkaif/my-repo:jma-2.0 .'
                  sh "echo $PASS | docker login -u $USER --password-stdin"
                  sh 'docker push shikalgarkaif/my-repo:jma-2.0'
    }
}

def deployApp() {
     echo "deploying the application..."
} 

return this